
var scene, camera, renderer, clock, deltaTime, totalTime;
var markerRoot1, markerRoot2;

var bottle, bottle_broken, box;

var dir_light;

initialize();
animate();

function initialize()
{
	scene = new THREE.Scene();

	let ambientLight = new THREE.AmbientLight( 0xcccccc, 0.5 );
	scene.add( ambientLight );
	
	//dir_light = new THREE.DirectionalLight(0xffffff, 0.5);

	//scene.add(
	//	dir_light
	//);

	camera = new THREE.Camera();
	scene.add(camera);

	renderer = new THREE.WebGLRenderer({
		antialias : true,
		alpha: true
	});
	renderer.setClearColor(new THREE.Color('lightgrey'), 0)
	renderer.setSize( 640, 480 );
	renderer.domElement.style.position = 'absolute'
	renderer.domElement.style.top = '0px'
	renderer.domElement.style.left = '0px'
	renderer.domElement.style.imageRendering = "pixelated";
	
	console.log(renderer.domElement.style.imageRendering);

	document.body.appendChild( renderer.domElement );

	clock = new THREE.Clock();
	deltaTime = 0;
	totalTime = 0;
	
	init_ar(renderer, camera);
	
	// build markerControls
	markerRoot1 = new THREE.Group();
	scene.add(markerRoot1);

	let markerControls1 = new THREEx.ArMarkerControls(arToolkitContext, markerRoot1, {
		type: 'pattern', patternUrl: "data/QAR.patt",
	})


	//var geometry = new THREE.BoxGeometry( 1, 1, 1 );
	//var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	//var cube = new THREE.Mesh( geometry, material );

	box = loadModel('models/', 'box.obj', 'box.mtl', markerRoot1,  true, true, 0x1E1E1E, false, false);
	
	box.object.renderOrder = 1;
	box.object.scale.set(0.65, 0.65, 0.65);
	box.object.position.y -= box.object.scale.y;
	//box.object.visible = false;

	box_back_face = loadModel('models/', 'box_back_face.obj', 'box_back_face.mtl', markerRoot1,  true, true, 0x000000, false, false);
	
	box_back_face.object.renderOrder = 1;
	box_back_face.object.scale.set(0.65, 0.65, 0.65);
	box_back_face.object.position.y -= box.object.scale.y;
	box_back_face.object.visible = false;

	var face = loadModel('models/', 'box_face.obj', 'box_face.mtl', markerRoot1, false, false, 0xffffff, false, false);
	face.object.position.y -= box.object.scale.y;
	face.object.scale.set(0.65, 0.65, 0.65);

	face.object.renderOrder = 2;

	
	bottle = loadModel('models/', 'bottle.obj', 'bottle.mtl', markerRoot1, 1, true, 0x7A2B15, true, false);
	bottle.object.scale.set(4.5, 4.5, 4.5);
	bottle.object.position.y -= box.object.scale.y;
	bottle.object.position.z += box.object.scale.z;
	bottle.object.rotation.x -= Math.PI/2;
	bottle.object.renderOrder = 2;

	bottle_broken = loadModel('models/', 'bottle_broken.obj', 'bottle_broken.mtl', markerRoot1, 1, true, 0x7A2B15, true, false, true);
	bottle_broken.object.scale.set(4.5, 4.5, 4.5);
	bottle_broken.object.position.y -= box.object.scale.y;
	bottle_broken.object.position.z += box.object.scale.z;
	bottle_broken.object.rotation.x -= Math.PI/2;
	bottle_broken.object.renderOrder = 2;


	console.log(bottle_broken.children);

	var light = new THREE.PointLight( 0xFCE57D);
	light.position.set( box.object.position.x, box.object.position.y, box.object.position.z);


	light.castShadow = true;

	light.shadow.mapSize.width = 1024;
	light.shadow.mapSize.height = 1024;

	light.shadow.camera.near = 500;
	light.shadow.camera.far = 4000;
	light.shadow.camera.fov = 1;

	markerRoot1.add( light );


	
	var plight = new THREE.PointLight( 0xFCE57D);
	plight.position.set( box.object.position.x  - 2, box.object.position.y - 2, box.object.position.z);


	plight.castShadow = true;

	plight.shadow.mapSize.width = 1024;
	plight.shadow.mapSize.height = 1024;

	plight.shadow.camera.near = 500;
	plight.shadow.camera.far = 4000;
	plight.shadow.camera.fov = 1;

	markerRoot1.add( plight );

	//var spotLightHelper = new THREE.SpotLightHelper( light );
	//markerRoot1.add( spotLightHelper );

	markerControls1.addEventListener("markerFound", (e)=>{		
		//console.log("marker being tracked");
		//console.log(markerRoot1.position); 
	})
}


function update()
{
	if(document.getElementById('slider').value > 0.1){
		bottle_broken.object.visible = true;
		bottle.object.visible = false;

		var center = getCenterPoint(bottle.children[0]).sub(new THREE.Vector3(0,.25,0));
		var distanceToMove = 10.0;

		for(var i = 0; i < bottle_broken.children.length; i++){
			var slider_val = document.getElementById('slider').value/1000;

			var meshPosition = getCenterPoint(bottle_broken.children[i]);

    		var direction = meshPosition.clone().sub(center).normalize();

    		var move_pos = direction.clone().multiplyScalar(distanceToMove);

			bottle_broken.children[i].position.lerpVectors(bottle_broken.children[i].position, move_pos, slider_val);
			
		}
	} else {
		bottle_broken.object.visible = false;
		bottle.object.visible = true;
	}


	if ( arToolkitSource.ready !== false )
		arToolkitContext.update( arToolkitSource.domElement );


}


function render()
{
	renderer.render( scene, camera );
}


function getCenterPoint(mesh) {
    var middle = new THREE.Vector3();
    var geometry = mesh.geometry;

    geometry.computeBoundingBox();

    middle.x = (geometry.boundingBox.max.x + geometry.boundingBox.min.x) / 2;
    middle.y = (geometry.boundingBox.max.y + geometry.boundingBox.min.y) / 2;
    middle.z = (geometry.boundingBox.max.z + geometry.boundingBox.min.z) / 2;

    mesh.localToWorld( middle );
    return middle;
}

function animate(){

	//dir_light.rotation.y += 1;

	requestAnimationFrame(animate);
	deltaTime = clock.getDelta();
	totalTime += deltaTime;
	update();
	render();
}

function loadModel(path, objectUrlName, objectmatUrlName, parent, render_order, color_write, color_hex, trans, wireframe, double_sided = false) {

    var container = new THREE.Object3D();
	var children_mesh = [];

    var onProgress = function (xhr) {
        if (xhr.lengthComputable) {
            var percentComplete = xhr.loaded / xhr.total * 100;
            //console.log( Math.round(percentComplete, 2) + '% downloaded' );
        }
    };

    var onError = function (xhr) { };

    THREE.Loader.Handlers.add(/\.dds$/i, new THREE.DDSLoader());

    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setPath(path);
    mtlLoader.load(objectmatUrlName, function (materials) {
		
		var material_capture;

        materials.preload();
        WondererMaterials = materials;

        var objLoader = new THREE.OBJLoader();


        objLoader.setMaterials(materials);


        objLoader.setPath(path);
        objLoader.load(objectUrlName, function (object) {

		container.add(object);
	
		object.traverse(function (child) {
				
			if (child instanceof THREE.Mesh) {
				
				children_mesh.push(child);

				child.castShadow = true;
				child.receiveShadow = true;
				child.material = new THREE.MeshPhysicalMaterial({color:color_hex});
				child.renderOrder = render_order; 
				child.material.colorWrite = color_write;
				child.material.transparent = trans;
				if(trans){child.material.opacity= 1.00;}
				child.material.wireframe = wireframe;
				if(double_sided){child.material.side = THREE.DoubleSide}
			}
		});
			

            container.castShadow = true;
            container.receiveShadow = true;
			parent.add(container);

        }, onProgress, onError);

    });

    return {object: container, children: children_mesh};
}